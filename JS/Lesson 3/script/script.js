const styles = ["Джаз", "Блюз"];   //создаю массив
styles.push("Рок-н-Ролл");  //добавляю в конец массива элемент "Рок-н-Ролл" 
console.log(styles);

if (styles.length % 2 == 0) {       //ищем середину массива
    let k = styles.length / 2;
} else {
    k = styles.length / 2 + 0.5;
}

styles.splice (k-1, 1, "Классика");     //заменяю в середине массива элемент на элемент "Классика"
console.log(k);
console.log(styles);

const firstItem = styles.shift();       //удаляю первый элемент массива
console.log(firstItem);
console.log(styles);

styles.unshift("Реп", "Регги");         // добавляю в начало массива два элемента
console.log(styles);