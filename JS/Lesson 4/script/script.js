
                            // Задача 1

// напиши функцию map(fn, array), которая принимает на вход функцию и массив , 
// и обрабатывает каждый элемент массива этой функцией, возвращая новый массив



const arr = [1, 2, 3, 4, 5, 6, 7];      //создаю массив

function fn(someArray) {                //описываем как входящая функция будет обрабатывать массив, 
    const arr1 = [];                    // и записываем данные в новый массив    
    for (let i=0; i<someArray.length; i++) {
        arr1[i] = someArray[i] +2; 
        
}
return arr1;
}

function map(array, func) {                 //основная функция, которая выводит результат на экран

    document.write(func(array));
}

map(arr, fn);


                                //Задача 2

function checkedAge(age) {                                      //переписываю функцию, используя тернарный оператор
    return age > 18 ? true : confirm("Родители разрешили?");
}

checkedAge(16); //проверка
checkedAge(39);