//Реализовать функцию для создания объекта "пользователь".
//Написать функцию createNewUser(), которая будет создавать 
//и возвращать объект newUser. При вызове функция должна спросить
 //у вызывающего имя и фамилию. Используя данные, введенные пользователем, 
 //создать объект newUser со свойствами firstName и lastName. Добавить в объект
 // newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
 //  соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
 //   Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
 //    Вывести в консоль результат выполнения функции.

  class CreateNewUser {
      constructor(name, lname) {
          this.firstName = name;
          this.lastName = lname; 
      }

      getLogin (){
          return this.firstName[0].toLowerCase() + this.lastName.toLowerCase(); 
      }
  }  

  const newUser = new CreateNewUser(prompt("Введите Ваше имя"), prompt("Введите Вашу фамилию"));
  
  console.log(newUser.getLogin());